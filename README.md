Certified Kubernetes Administrator (CKA) Program
================================================

* The Certified Kubernetes Administrator (CKA) program was created by the Cloud Native Computing Foundation (CNCF), in collaboration with The Linux Foundation, to help develop the Kubernetes ecosystem. As one of the highest velocity open source projects, Kubernetes use is exploding.

* The Cloud Native Computing Foundation is committed to growing the community of Kubernetes Administrators, thereby allowing continued growth across the broad set of companies and organizations that are using Kubernetes. Certification is a key step in that process, allowing certified administrators to quickly establish their credibility and value in the job market, and also allowing companies to more quickly hire high-quality teams to support their growth.

* The online exam consists of a set of performance-based items (problems) to be solved in a command line and candidates have 3 hours to complete the tasks.

* The Certification focuses on the skills required to be a successful Kubernetes Administrator in industry today. This includes these general domains and their weights on the exam:
```
 [] Application Lifecycle Management 8%
 [] Installation, Configuration & Validation 12%
 [] Core Concepts 19%
 [] Networking 11%
 [] Scheduling 5%
 [] Security 12%
 [] Cluster Maintenance 11%
 [] Logging / Monitoring 5%
 [] Storage 7%
 [] Troubleshooting 10%
```
* The cost is $300 and includes one free retake. For questions on the exam, please reach out to certificationsupport@cncf.io.
