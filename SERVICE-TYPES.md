```
--------------------------------------------------------------------------------
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
--------------------------------------------------------------------------------
busybox        LoadBalancer   10.96.96.171    <pending>     80:31934/TCP   3m19s
nginx-deploy   NodePort       10.107.37.20    <none>        80:31697/TCP   18m
webapp         ClusterIP      10.101.24.198   <none>        80/TCP         9m1s
busybox2       ExternalName   <none>          my.database.example.com   80/TCP 10m
```
