Create namespace
================
* kubectl create ns nginx

Create POD with manifest file in default and nginx namespaces with diffrent labels
==================================================================================
* kubectl run --generator=run-pod/v1 --image=nginx nginx --dry-run -o yaml >pod-manifeast.yaml
* kubectl apply -f pod-manifeast.yaml
* kubectl run --generator=run-pod/v1 --image=nginx nginx-dev1 --labels=env=dev --dry-run -o yaml >pod-labels-manifeast.yaml
* kubectl run --generator=run-pod/v1 --image=nginx nginx-prod --labels=env=prod --dry-run -o yaml >pod-labels-manifeast2.yaml

POD with limits and requests flags
==================================
* kubectl run --generator=run-pod/v1 --image=nginx nginx-prod2 --labels=env=prod --limits cpu=10m,memory=200Mi --requests cpu=5m,memory=100Mi
