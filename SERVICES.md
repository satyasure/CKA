SERVICES
========

**NodePort**

* kubectl expose nginx-deploy --port=80 --target-port=8080 --type=NodePort --dry-run -o yaml >>service-nodeport.yaml
* kubectl apply -f service-nodeport.yaml

**ClusterIp**

* kubectl expose deployment/webapp --port=80 --target-port=8080 --dry-run -o yaml >>service-clusterip.yaml
* kubectl apply -f service-clusterip.yaml

**LoadBalancer**

* kubectl expose deployment busybox --port=80 --target-port=8090 --type=LoadBalancer --dry-run -o yaml >>service-loadbalancer.yaml
* kubectl apply -f service-loadbalancer.yaml

**ExternalName**

* kubectl expose deployment busybox2 --port=80 --target-port=8091 --type=ExternalName --externalName --dry-run -o yaml >service-externalname.yaml
* Need to add one line like this to use external DNS records, externalName: my.database.example.com
* kubectl apply -f service-externalname.yaml
