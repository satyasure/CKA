 CreateDeployment using CLI and Manifest
========================================
* kubectl create deployment nginx-deploy --image=nginx --dry-run -o yaml >deployment-creation.yaml
* kubectl apply -f deployment-creation.yaml
* kubectl scale deployment/nginx-deploy --current-replicas=1 --replicas=2 --record
* kubectl set image deployment/nginx-deploy nginx-deploy=nginx:1.17.1 --record
* kubectl set image deployment/nginx-deploy nginx=nginx:1.17.1 --record
* kubectl set image deployment/nginx-deploy nginx=nginx:1.17.4 --record
* kubectl rollout history deployment nginx-deploy
* kubectl rollout undo deployment nginx-deploy --to-revision=1
