ETCD BACKUP & Restore
=====================
Get the member ID of etcd cluster

    sudo ETCDCTL_API=3 etcdctl --endpoints=https://127.0.0.1:2379 member list \
	--cacert=/etc/kubernetes/pki/etcd/ca.crt \
	--cert=/etc/kubernetes/pki/etcd/server.crt \
	--key=/etc/kubernetes/pki/etcd/server.key

To take Backup of etcd

    sudo ETCDCTL_API=3 etcdctl snapshot save mysnapshot.db \
	--endpoints=https://127.0.0.1:2379 \
	--cacert=/etc/kubernetes/pki/etcd/ca.crt \
	--cert=/etc/kubernetes/pki/etcd/server.crt \
	--key=/etc/kubernetes/pki/etcd/server.key

Restore from backup

    sudo ETCDCTL_API=3 etcdctl snapshot restore mysnapshot.db \
	–name master \
	–initial-cluster master=http://127.0.0.1:2380 \
	–initial-cluster-token etcd-cluster-1 \
	–initial-advertise-peer-urls http://127.0.0.1:2380 \
	–data-dir=/var/lib/etcd-new

Pre-requisites, Download and Install the etcd Binaries
======================================================
wget -q --show-progress --https-only --timestamping "https://github.com/etcd-io/etcd/releases/download/v3.4.10/etcd-v3.4.10-linux-amd64.tar.gz"

**Extract and install the etcd server and the etcdctl command line utility**

```
{
  tar -xvf etcd-v3.4.10-linux-amd64.tar.gz
  sudo mv etcd-v3.4.10-linux-amd64/etcd* /usr/local/bin/
  sudo mkdir -p /etc/etcd /var/lib/etcd
  sudo chmod 700 /var/lib/etcd
}
```
